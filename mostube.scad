module mostube(height = 10, outer_diameter = 10, inner_diameter = 0, gap = 0, roundcorner_radius = 0, x_anchor = 0, y_anchor = 0, z_anchor = 0, fn = 0) {

    assert(gap == 0 || roundcorner_radius == 0, "specify gap or roundcorner_radius as non-zero, or neither, but not both");
    

    $fn = fn ? fn : $fn;
    
    translate([x_anchor * (outer_diameter/2), y_anchor * (outer_diameter/2), z_anchor * (height/2)]) {
        minkowski() {
            difference() {
                cylinder(d = outer_diameter - ((roundcorner_radius + gap) * 2), h = height - ((roundcorner_radius + gap) * 2), center = true);
                if (inner_diameter < 0) {
                    cylinder(d = (outer_diameter + inner_diameter) + ((roundcorner_radius + gap) * 2), h = height +1, center = true);

                }
                if (inner_diameter > 0) {
                    cylinder(d = inner_diameter + ((roundcorner_radius + gap) * 2), h = height +1, center = true);
                }
            }
            sphere(r = roundcorner_radius);
        }
    }
}






// original mostube tests
translate([0, 0, 0]) {
    cube([10, 10, 10], center = true);
    translate([ 0, 0, 0]) color(   "cyan")  mostube(inner_diameter = -1);
    translate([ -20, 0, 0]) color(   "red")  mostube(outer_diameter = 20, x_anchor = 1, y_anchor = 1, z_anchor = 1);
    translate([20, 0, 0]) color( "green")  mostube(roundcorner_radius = 2);
    translate([40, 0, 0]) color(  "blue")  mostube(roundcorner_radius = 0.5, x_anchor = -1);
    translate([60, 0, 0]) color("orange")  mostube(roundcorner_radius = 0, x_anchor = -1, y_anchor = 1, z_anchor = 1);

    translate([0, 20, 0]) color("coral") mostube(inner_diameter = 5, roundcorner_radius = 0);
    translate([0, 40, 0]) color("plum") mostube(outer_diameter = 15, inner_diameter = -5, roundcorner_radius = 1);
    translate([0, 60, 0]) color("lime") mostube(outer_diameter = 20, inner_diameter = -5, height = 20, roundcorner_radius = 1, z_anchor = -0.5, fn = 5);
}


// mosrod tests
translate([0, -20, 0]) {
    color("green") {
        mostube();
    }
    
    translate([7.5, 0, 0]) {
        color("red") minkowski() {
            mostube(gap = 1);
            sphere(r = 1);
        }
        translate([1,1,1]) {
            color("green") minkowski() {
                mostube(gap = 1);
                sphere(r = 1);
            }
            translate([1,1,1]) {
                color("blue") mostube(roundcorner_radius = 1);
            }
        }
    }

    translate([20, 0, 0]) {
        roundiness = 1;
        minkowski() {
            difference() {
                mostube(gap = roundiness);
                translate([5, 0, 0]) {
                    rotate([0, 45, 0]) {
                        cube([10, 10, 10], center = true);
                    }
                }
            }
            sphere(r = roundiness);
        }
        translate([0,0,0]) {
            color("orange") minkowski() {
                difference() {
                    mostube(gap = roundiness, fn = 9);
                    translate([5, 0, 0]) {
                        rotate([0, 45, 0]) {
                            cube([10, 10, 10], center = true);
                        }
                    }
                }
                sphere(r = roundiness);
            }
        }
    }
}





// mostube tests
translate([0, -40, 0]) {

    color("orange") {
        mostube(outer_diameter = 10, inner_diameter = 5);
    }



    translate([20, 0, 0]) {
        color("red") mostube(outer_diameter = 20, inner_diameter = 10);
    }
        
        
    translate([40, 0, 0]) {
        color("green") mostube(outer_diameter = 20, inner_diameter = 10, gap = 1, fn=10);
    }


    translate([60, 0, 0]) {
        roundiness = 1;
        color("blue") minkowski() {
            mostube(outer_diameter = 20, inner_diameter = 10, gap = roundiness, height = 12, fn = 10);
            sphere(r = roundiness);
        }
    }
        



    translate([80, 0, 0]) {
        $fn = 25;
        color("cyan") difference() {
            mostube(outer_diameter = 20, inner_diameter = 10, height = 30, fn = 13);
            translate([10, 0, 0]) {
                rotate([90, 0, 0]) {
                    cylinder(d = 20, h = 21, center = true);
                }
            }
        }
    }




    translate([100, 0, 0]) {
            $fn = 25;
            roundiness = 1;

            color("purple") minkowski() {
                difference() {
                    mostube(outer_diameter = 20, inner_diameter = 10, gap = roundiness, fn = 12);
                    translate([10, 0, 0]) {
                        rotate([90, 0, 0]) {
                            cylinder(d = 5, h = 20, center = true);
                        }
                    }
                }
                sphere(r = roundiness);
            }
        }






         translate([80, 0, 0]) {
            $fn = 25;
            roundiness = 1;

            color("coral") minkowski() {
                difference() {
                    mostube(outer_diameter = 20, inner_diameter = 10, height = 30, gap = roundiness, fn = 6);
                    translate([10, 0, 0]) {
                        rotate([90, 0, 0]) {
                        cylinder(d = 20, h = 20, center = true);
                        }
                    }
                }
                sphere(r = roundiness);
            }
        }
        
    




}