module mosrod(diameter = 10, height = 10, x_anchor = 0, y_anchor = 0, z_anchor = 0, gap = 0, fn = 0) {
    $fn = fn ? fn : $fn;
    translate([(diameter / 2) * x_anchor, (diameter / 2) * y_anchor, (height / 2) * z_anchor]) {
        cylinder(d = diameter - (gap * 2), h = height - (gap * 2), center = true);
    }
}

module mostube(outer_diameter = 10, inner_diameter = 5, height = 10, x_anchor = 0, y_anchor = 0, z_anchor = 0, gap = 0, fn = 0) {
    $fn = fn ? fn : $fn;
    translate([(outer_diameter / 2) * x_anchor, (outer_diameter / 2) * y_anchor, (height / 2) * z_anchor]) {
        difference() {
            cylinder(d = outer_diameter - (gap * 2), h = height - (gap * 2), center = true);
            cylinder(d = inner_diameter + (gap * 2), h = height - (gap * 2) + 0.01, center = true);
        }
    }
}






translate([0, -60, 0]) {
    // mosrod tests
    mosrod();
    
    translate([7.5, 0, 0]) {
        minkowski() {
            mosrod(gap = 1);
            sphere(r = 1);
        }
    }
    
    translate([20, 0, 0]) {
        roundiness = 1;
        minkowski() {
            difference() {
                mosrod(gap = roundiness);
                translate([5, 0, 0]) {
                    rotate([0, 45, 0]) {
                        cube([10, 10, 10], center = true);
                    }
                }
            }
            sphere(r = roundiness);
        }
    }



    // mostube tests
    translate([0, 20, 0]) {
        color("orange") mostube();
        translate([20, 0, 0]) {
            color("red") mostube(outer_diameter = 20, inner_diameter = 10);
        }
        translate([40, 0, 0]) {
            color("green") mostube(outer_diameter = 20, inner_diameter = 10, gap = 1, fn=10);
        }
        translate([60, 0, 0]) {
            roundiness = 1;
            color("blue") minkowski() {
                mostube(outer_diameter = 20, inner_diameter = 10, gap = roundiness, height = 12, fn = 10);
                sphere(r = roundiness);
            }
        }
        
        translate([80, 0, 0]) {
            $fn = 25;
            color("cyan") difference() {
                mostube(outer_diameter = 20, inner_diameter = 10, height = 30, fn = 12);
                translate([10, 0, 0]) {
                    rotate([90, 0, 0]) {
                        cylinder(d = 20, h = 20, center = true);
                    }
                }
            }
        }

        translate([100, 0, 0]) {
            $fn = 25;
            roundiness = 1;

            color("purple") minkowski() {
                difference() {
                    mostube(outer_diameter = 20, inner_diameter = 10, gap = roundiness, fn = 12);
                    translate([10, 0, 0]) {
                        rotate([90, 0, 0]) {
                            cylinder(d = 5, h = 20, center = true);
                        }
                    }
                }
                sphere(r = roundiness);
            }
        }
                

         translate([80, 0, 0]) {
            $fn = 25;
            roundiness = 1;

            color("coral") minkowski() {
                difference() {
                    mostube(outer_diameter = 20, inner_diameter = 10, height = 30, gap = roundiness, fn = 6);
                    translate([10, 0, 0]) {
                        rotate([90, 0, 0]) {
                        cylinder(d = 20, h = 20, center = true);
                        }
                    }
                }
                sphere(r = roundiness);
            }
        }
        
    }
}